import os
import time
import re
import json
import sys
from requests import Session
# from seleniumwire import webdriver
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from subprocess import call
from random import randint


## DeathByCaptcha
username = "eduardohayashi"
password = "rj6FlGZZ1e8ggC4K"

geckodriver_version = 17
firefox_version = 55
proxy = None #'http://35.237.68.201:8008'
profile_dir = '/home/eduardo/firefox/'

if not os.path.isdir(profile_dir):
    os.makedirs(profile_dir)



def fillCaptcha(driver):
    try:
        src = driver.find_element_by_xpath("//div[@name='recaptcha']//iframe").get_attribute('src')
        googlekey = re.findall('k=.*&co', src)[0].replace('k=','').replace('&co', '')

        print("Iniciando DeathByCaptcha - ", googlekey)

        Captcha_dict = {
                'proxy': proxy,
                'proxytype': 'HTTP',
                'googlekey': googlekey,
                'pageurl': site
        }

        json_Captcha = json.dumps(Captcha_dict)
        files = {
            'username': (None, username),
            'password': (None, password),
            'type': (None, '4'),
            'token_params': (None, json_Captcha),
        }

        proxies = {
            'http': proxy,
        }

        session = Session()
        session.proxies = proxies

        # response = session.post('http://api.dbcapi.me/api/captcha', files=files)
        # print(response.text)
        # time.sleep(defalt_interval)

        # captcha_id = dict(x.split('=') for x in response.text.split('&'))['captcha']
        # response_text = None
        # while not response_text:
        #     time.sleep(defalt_interval)
        #     response = session.get('http://api.dbcapi.me/api/captcha/{}'.format(captcha_id))
        #     response_text = dict(x.split('=') for x in response.text.split('&'))['text']
        #     is_correct = dict(x.split('=') for x in response.text.split('&'))['is_correct']

        #     print('response_text', response_text)

        response_text = "\"03AMGVjXiL4hUNfxlPq7fS4pmB7K5OgEIxbjf_w-VGHvDy-PEOroeOmOsFuvZhzlp1mCuW0Oy2n9T-Ngwh6JQZDuf1cQOncMC3t8t1AB9E2HkmrA73in2CmI2f60huoQzRmWaR-Bf7zdzu5Pw4_CI8GoTfllCqEnSXrWkjYA9ckezC25HKIHT-9mwYNY9oC2BS54JNUrArBnDkzfML0dPlQCaKJ9bGj2R7j8wN6WbKrJMs3Xz1_eAc1iwM-rLm29pVx-mR4mAgpZ5UEJmddmkWKyQ7czZdxsQ1BlNMTfNzq9a2eQ60kMgKkdr5yTp2mPOKlBKpLuRo-1H-TRZgqxMoiVo1SI6olZbYbw\""

        captcha = response_text
        print ("CAPTCHA solved: %s" % (captcha))

        if captcha:
            textarea = driver.find_element_by_xpath("//textarea[@name='g-recaptcha-response']")
            driver.execute_script("arguments[0].textContent = '{}';".format(captcha), textarea)
            form = driver.find_element_by_xpath("//form")
            driver.execute_script("arguments[0].onSubmit = '';", form)
            form.submit()
            # time.sleep(defalt_interval)
        import ipdb
        ipdb.set_trace()

    except Exception as e:
        # Access to DBC API denied, check your credentials and/or balance
        print ("error: Access to DBC API denied," +
               "check your credentials and/or balance - " + str(e))
        driver.close()


def hasCaptcha(driver, object):
    try:
        modal = driver.find_elements_by_xpath(object)
        # print(modal)
        for m in modal:
            if m.is_displayed():
                return True
    except:
        print('No captcha')
        return False

def openWebDriverNew():
    driver = webdriver.Firefox()
    return driver


def openWebDriver():
    options = Options()
    options.add_argument("-headless")
    options.add_argument("-profile")
    options.add_argument(profile_dir)

    capabilities = {
                    "httpProxy":proxy,
                    "ftpProxy":proxy,
                    "sslProxy":proxy,
                    "proxyType":"manual",
                    "browserName": "firefox",
                    "marionette": True,
                    "acceptInsecureCerts": True,
                 }

    profile = webdriver.FirefoxProfile()
    # Disable all images from loading, speeds page loading
    # http://kb.mozillazine.org/Permissions.default.image
    profile.set_preference('permissions.default.image', 3)
    profile.set_preference('dom.ipc.plugins.enabled.libflashplayer.so', 'false')
    profile.set_preference("media.volume_scale", "0.0")
    profile.set_preference('media.autoplay.enabled', False)
    profile.set_preference('browser.link.open_newwindow', 3)
    profile.update_preferences()

    driver = webdriver.Firefox(
        options=options,
        firefox_profile=profile,
        executable_path='/opt/geckodriver/{}/geckodriver'.format(geckodriver_version),
        firefox_binary='/opt/firefox/{}/firefox/firefox'.format(firefox_version),
        capabilities=capabilities,
        service_args=["--marionette-port", "2828"]
        )
    return driver


#def crex24(driver):
#    print('starting Crex24')
#    site = 'https://crex24.com/faucets'
#    driver.get(site)
#    while True:
#        try:
#            login = driver.find_element_by_xpath("//a[@class='account_control_elem']")
#            break
#        except:
#            print("Aguardando Login")
#            time.sleep(1)
#
#
#    for i in range(0,11):
#        try:
#
#            coin = driver.find_element_by_xpath("//div[@class='content_other_pages']/div/table/tbody/tr[{}]/td[1]".format(i)).text
#            if not coin:
#                continue
#
#
#            print('{} - {}'.format(str(i),coin), end='')
#            sys.stdout.flush()
#            button = driver.find_element_by_xpath("//div[@class='content_other_pages']/div/table/tbody/tr[{}]/td[4]".format(i))
#            if  button.text != "Get":
#                print(' - ' + button.text)
#                time.sleep(0.1)
#                continue
#
#            button.click()
#            if hasCaptcha(driver, "//div[@class='ReactModalPortal']"):
#                call(['/usr/bin/notify-send','CREX24'])
#                print('Modal encontrado. Aguardando')
#
#            t=30
#            while hasCaptcha(driver, "//div[@class='ReactModalPortal']") and t > 0:
#                time.sleep(1)
#                print(t, end=' ')
#                t = t-1
#                sys.stdout.flush()
#
#        except Exception as e:
#            print('Não foi possivel clicar no elemento Get - ' + str(e))
#            time.sleep(1)
#            continue




def freebitcoin(driver):
    print('starting Freebitcoin')
    site = 'https://freebitco.in/?op=home'
    driver.get(site)
    time.sleep(1.2)

    while True:
        try:
            balance = driver.find_element_by_id("balance")
            print("Balance: {}".format(balance.text))
            break
        except:
            print("Aguardando Login")
            time.sleep(1)

    try:


        if not hasCaptcha(driver, "//input[@id='free_play_form_button']"):
            print('AINDA NÃO!')
            return

        # call(['/usr/bin/notify-send','Freebitcoin'])
        # print('Modal encontrado. Aguardando')

        reward_points = driver.execute_script("RedeemRPProduct('free_points_100')")
        print("Spending RP", reward_points)
        time.sleep(1)
        action = ActionChains(driver)
        print('action', action)
        action.move_to_element(driver.find_element_by_xpath("//input[@id='free_play_form_button']"))
        # time.sleep(1)
        driver.find_element_by_tag_name('body').send_keys(Keys.END)
        i=0
        while hasCaptcha(driver, "//input[@id='free_play_form_button']"):
            i = i+1
            if i==10:
                return
            print("Trying to click()")
            time.sleep(1)
            driver.find_element_by_xpath("//input[@id='free_play_form_button']").click()
            action.click().perform()

        time.sleep(5)
    except:
        print("Freebitcoin error!")
        time.sleep(1)


def cointiply(driver):
    print('starting Cointiply')
    time.sleep(1)
    site = 'https://cointiply.com/home?intent=faucet'
    driver.get(site)
    time.sleep(1)
    while True:
        try:
            balance = driver.find_element_by_xpath("//div[@class='headertopright col-md-4']//div")
            print("Balance: {}".format(balance.text))
            break
        except:
            print("Aguardando Login")
            time.sleep(1)

    try:
        button = driver.find_element_by_xpath("//button[contains(text(),'Roll & Win')]")
        button.click()
        call(['/usr/bin/notify-send','Cointiply'])
        print('Captcha encontrado. Aguardando')

        t=30
        while hasCaptcha(driver, "//div[@class='md-input-container md-theme-default md-has-select md-has-value']") and t > 0:
            time.sleep(1)
            print(t, end=' ')
            t = t-1
            sys.stdout.flush()

        time.sleep(5)
    except:
        print("AINDA NÃO!")
        time.sleep(1)

def bitfun(driver):

    print('starting bitfun')
    site = 'https://bitfun.co/offers'
    driver.get(site)
    time.sleep(1)

    try:

        balance = driver.find_element_by_xpath("//div[@class='claimNow']")
        balance_int = [int(s) for s in balance.text.split() if s.isdigit()]
        print("Balance: {}".format(balance.text))
    except:
        print("bitfun error")
        return

    try:

        if balance_int[0] < 9:
            print('Muito cedo, vamos aguardar...')
            return
        claim = driver.find_element_by_xpath("//button[@class='btn btn-primary btn-lg claimButton']")
        call(['/usr/bin/notify-send','BitFun'])
        print('Captcha encontrado. Aguardando')
        claim.click()

        t=30
        while t>0:
            time.sleep(1)
            print(t, end=' ')
            t = t-1
            sys.stdout.flush()

        # while hasCaptcha(driver, "//div[@id='ClaimModal']//div[@class='modal-content']"):
        #     time.sleep(0.5)
        time.sleep(1)
    except:
        print("AINDA NÃO!")
        time.sleep(1)




def adbtc(driver):
    print('starting adbtc')
    site = 'https://adbtc.top/surf/browse/93caf3aa4eeb4db511e7d0838792e73b'
    driver.get(site)
    time.sleep(10)

    try:
        balance = driver.find_elements_by_class_name('small_rating')[0].get_attribute('innerHTML')
        balance_int = [int(s) for s in balance.split() if s.isdigit()]
        print("Balance: {}".format(balance))
    except:
        print("adbtc error")
        return

    try:
        print('try...')
        if driver.find_elements_by_xpath("//div[@id='nenado']/parent::*//div//a")[2].text != '':
            driver.find_elements_by_xpath("//div[@id='nenado']/parent::*//div//a")[2].click()

            time.sleep(5)
            wait = driver.find_elements_by_id('tmr')[0].text
            wait_int = [int(s) for s in wait.split() if s.isdigit()]
            print(wait)
            if wait:
                print(f"Wait +{wait_int[0]}")
                time.sleep(wait_int[0]+10)

            driver.switch_to.window(driver.window_handles[0])
            driver.switch_to.window(driver.window_handles[1])
            driver.close()
            driver.switch_to.window(driver.window_handles[0])
            time.sleep(3)

        if not driver.find_elements_by_xpath("//div[@id='nenado']/parent::*//div//a"):
            print(' Nada pra surfar. Aguardando')
            time.sleep(10)

        time.sleep(1)
        driver.switch_to.window(driver.window_handles[0])
    except:
        print("adbtc ERROR!")


def moonbitcoin(driver):
    name = 'Moonbitcoin'
    print('starting ', name)
    site = 'https://moonbit.co.in/faucet'
    driver.get(site)
    time.sleep(0.1)

    try:
        balance = driver.find_elements_by_xpath("//html[1]/body[1]/div[1]/div[1]/div[3]/div[2]/p[2]/span[1]")[0]
        balance_int = float(balance.text)
        print("Balance: {}".format(balance.text))
    except:
        print(name, "error")
        return

    try:
        if balance_int < 5e-08:
            print('Muito cedo, vamos aguardar mais um pouco')
            return
        button = driver.find_element_by_xpath("//span[contains(text(),'claim now')]")
        print(button.text)
        button.click()
        call(['/usr/bin/notify-send', name])
        print('Captcha encontrado. Aguardando')
        t=30
        while t>0:
            time.sleep(1)
            print(t, end=' ')
            t = t-1
            sys.stdout.flush()
        print()

    except:
        print("AINDA NÃO!")
        time.sleep(1)


def moondogecoin(driver):
    name = 'MoonDogeCoin'
    print('starting', name)
    site = 'http://moondoge.co.in/faucet'
    driver.get(site)
    time.sleep(0.1)

    try:
        balance = driver.find_elements_by_xpath("//html[1]/body[1]/div[1]/div[1]/div[3]/div[2]/p[2]/span[1]")[0]
        balance_int = float(balance.text)
        print("Balance: {}".format(balance.text))
    except:
        print(name, "error")
        return

    try:
        if balance_int < 0.1:
            print('Muito cedo, vamos aguardar mais um pouco')
            return
        button = driver.find_element_by_xpath("//span[contains(text(),'claim now')]")
        print(button.text)
        button.click()
        call(['/usr/bin/notify-send', name])
        print('Captcha encontrado. Aguardando')
        t=30
        while t>0:
            time.sleep(1)
            print(t, end=' ')
            t = t-1
            sys.stdout.flush()
        print()

    except:
        print("AINDA NÃO!")
        time.sleep(1)


def moonlitecoin(driver):
    name = 'MoonLiteCoin'
    print('starting', name)
    site = 'http://moonliteco.in/faucet'
    driver.get(site)
    time.sleep(0.1)

    try:
        balance = driver.find_elements_by_xpath("//html[1]/body[1]/div[1]/div[1]/div[3]/div[2]/p[2]/span[1]")[0]
        balance_int = float(balance.text)
        print("Balance: {}".format(balance.text))
    except:
        print(name, " error")
        return

    try:
        if balance_int < 0.000005:
            print('Muito cedo, vamos aguardar mais um pouco')
            return
        button = driver.find_element_by_xpath("//span[contains(text(),'claim now')]")
        print(button.text)
        button.click()
        call(['/usr/bin/notify-send', name])
        print('Captcha encontrado. Aguardando')
        t=30
        while t>0:
            time.sleep(1)
            print(t, end=' ')
            t = t-1
            sys.stdout.flush()
        print()

    except:
        print("AINDA NÃO!")
        time.sleep(1)


def moonbitcoincash(driver):
    name = 'MoonBitcoinCash'
    print('starting', name)
    site = 'http://moonbitcoin.cash/faucet'
    driver.get(site)
    time.sleep(0.1)

    try:
        balance = driver.find_elements_by_xpath("//html[1]/body[1]/div[1]/div[1]/div[3]/div[2]/p[2]/span[1]")[0]
        balance_int = float(balance.text)
        print("Balance: {}".format(balance.text))
    except:
        print(name, " error")
        return

    try:
        if balance_int < 0.0000005:
            print('Muito cedo, vamos aguardar mais um pouco')
            return
        button = driver.find_element_by_xpath("//span[contains(text(),'claim now')]")
        print(button.text)
        button.click()
        call(['/usr/bin/notify-send', name])
        print('Captcha encontrado. Aguardando')
        t=30
        while t>0:
            time.sleep(1)
            print(t, end=' ')
            t = t-1
            sys.stdout.flush()
        print()

    except:
        print("AINDA NÃO!")
        time.sleep(1)



def moondash(driver):
    name = 'MoonDash'
    print('starting', name)
    site = 'http://moondash.co.in/faucet'
    driver.get(site)
    time.sleep(0.1)

    try:
        balance = driver.find_elements_by_xpath("//html[1]/body[1]/div[1]/div[1]/div[3]/div[2]/p[2]/span[1]")[0]
        balance_int = float(balance.text)
        print("Balance: {}".format(balance.text))
    except:
        print(name, " error")
        return

    try:
        if balance_int < 0.000005:
            print('Muito cedo, vamos aguardar mais um pouco')
            return
        button = driver.find_element_by_xpath("//span[contains(text(),'claim now')]")
        print(button.text)
        button.click()
        call(['/usr/bin/notify-send', name])
        print('Captcha encontrado. Aguardando')
        t=30
        while t>0:
            time.sleep(1)
            print(t, end=' ')
            t = t-1
            sys.stdout.flush()
        print()

    except:
        print("AINDA NÃO!")
        time.sleep(1)


def bonusbitcoin(driver):
    print('starting BonusBitcoin')
    site = 'https://bonusbitcoin.co/faucet'
    driver.get(site)
    time.sleep(1)

    try:

        balance = driver.find_element_by_xpath("//input[@id='BalanceInput']").get_attribute('value')
        print("Balance: {}".format(balance))
    except:
        print("BonusBitcoin error")
        return

    try:
        claim = driver.find_element_by_xpath("/html[1]/body[1]/div[3]/div[2]/div[2]/form[1]/button[2]")

        if claim.text != "Claim now!":
            print('Ainda não')
            return
        call(['/usr/bin/notify-send','BonusBitcoin'])
        print('Captcha encontrado. Aguardando')
        t=30
        while t>0:
            time.sleep(1)
            print(t, end=' ')
            t = t-1
            sys.stdout.flush()
    except:
        print("AINDA NÃO!")
        time.sleep(1)


def bololex(driver):
    print('starting bololex')
    # site = 'https://bololex.com/game/airdrop'
    site = 'https://bololex.com/voting'
    driver.get(site)
    time.sleep(1)

    try:

        # img = driver.find_element_by_xpath("//main/div[1]/div[1]/div[2]//div/div[@class='voting_list_container']/div[1]/a/img[contains(@alt, 'RDCT')]/../../../../div[2]/div[2]/div[1]/img").get_attribute('src')
        # div = driver.find_element_by_xpath("//div[@class='energies_info']").text[-8:]
        print("time: {}".format(div))
        r = randint(1,10)
        print(r)
        if r == 10:
            print("trying voting")
            print(driver.find_element_by_xpath("//main/div[1]/div[1]/div[2]//div/div[@class='voting_list_container']/div[1]/a/img[contains(@alt, 'BTC2')]/../../../../div[2]/div[2]/div[1]/img").click())

# from faucet import *
# driver = openWebDriverNew()
# driver.get('https://bololex.com/game/airdrop')
#
# el = driver.find_elements_by_xpath("//canvas")[]
# action = webdriver.common.action_chains.ActionChains(driver)
# action.move_to_element_with_offset(el, 372, 372).click().perform()
#
# action.move_to_element(el).perform()

    except:
        pass


def do4stake(driver):
    try:
        print('starting 4stake')
        site = 'https://dashboard.4stake.com/recuperacao'
        driver.get(site)
        time.sleep(10)
    except:
        pass

def stakecube(driver):
    print('starting stakecube')
    site = 'https://stakecube.net/app/exchange/'
    driver.get(site)
    time.sleep(20)





def main():
    try:
        k=1
        driver = None
        while True:

            if not driver:
                print('Opening Firefox webdriver')
                driver = openWebDriver()


            stakecube(driver)
            freebitcoin(driver)
            do4stake(driver)
            # adbtc(driver)
            # bololex(driver)


            # crex24(driver)


            # cointiply(driver)

            # bonusbitcoin(driver)
            # moonbitcoin(driver)
            # moondogecoin(driver)
            # moonlitecoin(driver)
            # moonbitcoincash(driver)
            # moondash(driver)
            # bitfun(driver)


            t=60
            while t>0:
                time.sleep(1)
                print(t, end=' ')
                t = t-1
                sys.stdout.flush()
            print()

            ## Desativados

            print('k', k)
            k = k+1
            if k%120 == 0:
                driver.quit()
                driver = None

    except Exception as e:
        driver.quit()
        print("Erro MAIN", str(e))
        main()


if __name__ == '__main__':
    main()


#
#
#
# from faucet import *
