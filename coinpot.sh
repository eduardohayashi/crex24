
GAIN=0
TOTAL=0
SEQ=1
MAXSEQ=0
LASTRESULT=99
LOST=0
INITIALSTAKE=1
STAKE=1

while true; do
  HIGHLOW=$((RANDOM%2))

  if [ $SEQ -gt 6 ] && [ $RESULT -eq 0 ]; then
    STAKE=$((STAKE*2))
    if [ $SEQ -gt 13 ]; then
      LOST=$((LOST+1))
      STAKE=$INITIALSTAKE
      /usr/bin/notify-send "COINPOT LOST - 1"
    fi
  else
    STAKE=$INITIALSTAKE
  fi


  RESPONSE=`curl -s 'https://coinpot.co/api/games/service.svc/PerformRoll' \
    -H 'Connection: keep-alive' \
    -H 'Pragma: no-cache' \
    -H 'Cache-Control: no-cache' \
    -H 'Accept: application/json, text/javascript, */*; q=0.01' \
    -H 'X-Requested-With: XMLHttpRequest' \
    -H 'User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.129 Safari/537.36' \
    -H 'Content-Type: application/json; charset=UTF-8' \
    -H 'Origin: https://coinpot.co' \
    -H 'Sec-Fetch-Site: same-origin' \
    -H 'Sec-Fetch-Mode: cors' \
    -H 'Sec-Fetch-Dest: empty' \
    -H 'Referer: https://coinpot.co/multiplier' \
    -H 'Accept-Language: pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7' \
    -H 'Cookie: __cfduid=d2f31cb2d121b32c03f3f0264c586d4f21591874092; SRVNAME=C17; session62=G0dvMHDn3OE38FcYo2DpMTwiKs1WGDxxoCNuDjr0stkoYReoRyXObv9IFtC7zIX8mXwxhugc5AAANQj7IP5IC9' \
    --data-binary '{"request":{"highLow":"'$HIGHLOW'","multiplier":2,"clientSeed":"0fa241a811a2","stake":'$STAKE'}}' \
    --compressed`


  TEXT=`jq '.d.resultDetails' <<<$RESPONSE`
  RESULT=`jq '.d.roll.rollResult' <<<$RESPONSE`
  BALANCE=`jq '.d.tokenBalance' <<<$RESPONSE`

  (("$RESULT" == "$LASTRESULT")) && SEQ=$((SEQ+1)) || SEQ=1
  (($SEQ > $MAXSEQ)) && MAXSEQ=$SEQ
  LASTRESULT=$RESULT

  TOTAL=$((TOTAL+1))
  if [ $RESULT -eq 1 ]; then
    GAIN=$((GAIN+1))
  fi

  printf "$TOTAL $LOST $HIGHLOW $MAXSEQ $SEQ $STAKE "
  printf $((GAIN*100/TOTAL))'%% '
  printf "$BALANCE "
  echo $TEXT
  #sleep 0.1

done

# {
#   "d": {
#     "__type": "PerformRollResponse:#CoinPot.Web.API.Games",
#     "result": true,
#     "resultDetails": "You won 2 tokens",
#     "defaultClientSeed": "412a523efcd6",
#     "roll": {
#       "__type": "Roll:#CoinPot.Web.API.Games",
#       "clientSeed": "a22f316eecb2",
#       "highLow": 0,
#       "multiplier": 2,
#       "profit": 2,
#       "rollResult": 1,
#       "rollTimestampUTC": "2020-06-12 09:04:26",
#       "rolledValue": 80,
#       "serverSeed": "62203fc312ab248b7b08c16650759eac3a42b6b2e32f7598",
#       "serverSeedHash": "6cdac61deb9ef38035f2c083e4e5d9b47bac31f29f71fc954d2395e020686a78",
#       "stake": 1
#     },
#     "serverSeedHash": "818421a1cc5e558bd9efef0acb121272ccd70baaa838ceda0f017003e4bdab49",
#     "throttle": false,
#     "tokenBalance": 437
#   }
# }
